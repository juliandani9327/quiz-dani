const express = require("express");
const bodyParser = require("body-parser");
const appServer = express();
const app = express();
const port = 5050;

app.use(bodyParser.json());
appServer.use(bodyParser.json());
const CheckJson = bodyParser.json();

app.get("/dani", (req, res) => {
  res.send("Belajar Web 3");
});

app.post("/methodpost", CheckJson, (req, res) => {
  res.json({
    Nama: "Julian Dani",
    Kelas: "3SIP1",
  });
});

app.post("/tampil", CheckJson, function (req, res) {
  var nama = "Julian Dani";
  var kelas = "3SIP1";
  res.send("nama : " + nama + "," + kelas);
});

// app.post('/tampil', CheckJson, (req, res) => {
//     res.send({
//       body: req.body.nama
//     });
//   });

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
